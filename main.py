from time import sleep

from machine import Pin
led = Pin(2, Pin.OUT)

# Blink rate
BRate=0.25

# Translation table for morse
CODE = {
    ' ': '_', "'": '.----.', '(': '-.--.-', ')': '-.--.-', ',': '--..--', '-': '-....-', '.': '.-.-.-', '/': '-..-.',
    '0': '-----', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.',
    ':': '---...', ';': '-.-.-.', '?': '..--..', 
    'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 
    'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 
    'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 
    'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..', '_': '..--.-'
}


def morse_dash():
    led.value(1)
    sleep(4*BRate)
    led.value(0)
    

def morse_pause():
    sleep(BRate)


def morse_dot():
    led.value(1)
    sleep(BRate)
    led.value(0)


def convertToMorseCode(sentence):
    sentence = sentence.upper()
    encodedSentence = ""
    for character in sentence:
        encodedSentence += CODE[character] + "" 
    return encodedSentence


while True:

    sentence = "SOS WE ARE SINKING"
    encodedSentence = convertToMorseCode(sentence)

    for i in encodedSentence:
        if i == ".":
            morse_dot()
        elif i == "-":
            morse_dash()
        morse_pause()
