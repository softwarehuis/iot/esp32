# Following instructions at https://randomnerdtutorials.com/micropython-mqtt-esp32-esp8266/


# Requirements
```
pip install adafruit-ampy
pip install esptool
```

# Installing micropython on the ESP
```
python -m esptool --chip esp32 --port /dev/ttyUSB0 erase_flash
python -m esptool --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32-20220618-v1.19.1.bin
```

# Opening an interactive python on linux:

```
screen /dev/ttyUSB0 115200
```

# Alternative IDE

You can use [Arduino Lab](https://labs.arduino.cc/en/labs/micropython) simply download and unzip and then run `./arduino-lab-micropython-ide`

# List content of the esp 32

```
ampy -p /dev/ttyUSB0 ls
```

# Upload main.py

```
ampy -p /dev/ttyUSB0 put main.py
```

__Restart the esp32 by pushing the button on the board__


